const protocPlugin = require("protoc-plugin");
const pascalcase = require("pascalcase");
const snakecase = require("snake-case");

function findTypescriptType(proto, type) {
  if (type.typeName) {
    console.error(`finding ${type.typeName}`);
    const t = find(proto, type.typeName);
    if (t) {
      return type.typeName.replace(`.${proto.pb_package}`, "pb");
    }
  }

  console.error(type.type, type.name);

  switch (type.type) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 13:
      return "number";
    case 9:
      return "string";
    default:
      return "any";
  }
}

function find(proto, typeName) {
  // console.error(`looking for '${typeName}'`);
  function _find(c, prefix = "") {
    for (let i = 0, type = c[0]; type; type = c[++i]) {
      const thisName = `.${proto.pb_package}${prefix}.${type.name}`;

      // console.error(`comparing to ${thisName}`);
      if (typeName === thisName) {
        return type;
      } else if (typeName.indexOf(`${thisName}.`) >= 0) {
        return _find(type.nestedTypeList, `.${type.name}`);
      }
    }
  }

  return _find(proto.messageTypeList);
}

protocPlugin(protos => {
  const files = [];

  protos.map(proto => {
    console.error(`Working on ${proto.pb_package}.`);
    proto.serviceList.forEach(function(service) {
      const content = [];
      const fileName = proto.name.replace(".proto", "_pb"); // booking_service_pb
      content.push("// GENERATED CODE -- DO NOT EDIT!\n");
      content.push(`import Client from './Client'`);
      content.push(
        `import { ${service.name} } from './_proto/${fileName}_service'`
      );
      content.push(`import * as pb from './_proto/${fileName}'`);

      content.push("");

      content.push(`export * from './_proto/${fileName}'`);
      content.push(`export default class ${service.name}Api extends Client {`);
      const functions = [];
      service.methodList.forEach(function(method) {
        const funcContent = [];

        const inputType = find(proto, method.inputType);
        const outputType = find(proto, method.outputType);
        const args = inputType.fieldList.map(field => {
          const name = field.name;
          const type = findTypescriptType(proto, field);

          return `${name}: ${type}`;
        });

        const setVars = inputType.fieldList.map(field => {
          const name = field.name;
          return `req.set${pascalcase(name)}(${name})`;
        });

        var returnType = `pb.${outputType.name}`;
        var requestType = `pb.${inputType.name}`;

        funcContent.push(
          `  public ${method.name}(${args.join(
            ", "
          )}): Promise<${returnType}> {`
        );

        funcContent.push(`    const req = new ${requestType}()`);
        funcContent.push(`    ${setVars.join("\n    ")}`);
        funcContent.push(
          `    return this.request<${returnType}>(${service.name}.${
            method.name
          }, req)`
        );
        funcContent.push(`  }`);

        functions.push(funcContent.join("\n"));
      });

      content.push(functions.join("\n\n"));

      content.push(`}`);

      files.push({
        name: `${service.name}Api.ts`,
        content: content.join("\n")
      });
    });
  });

  let content = ["// GENERATED CODE -- DO NOT EDIT!", ""];
  const imports = [];
  const decs = [];
  const assign = [];

  protos.map(proto => {
    proto.serviceList.map(service => {
      // console.log(service);
      imports.push(`import * as ${service.name} from './${service.name}Api';`);
      imports.push(`import ${service.name}Api from './${service.name}Api';`);

      decs.push(`  ${snakecase(service.name)}: ${service.name}Api`);

      assign.push(
        `    this.${snakecase(service.name)} = new ${service.name}Api(host)`
      );
    });
  });

  content = content.concat(imports);
  content.push("export default class Api {");
  content = content.concat(decs);
  content.push("  constructor(host: string) {");
  content = content.concat(assign);
  content = content.concat(["  }", "}"]);

  files.push({
    name: "index.ts",
    content: content.join("\n")
  });

  files.push({
    name: "Client.ts",
    content: `// GENERATED CODE -- DO NOT EDIT!

import { Message } from "google-protobuf";
import { grpc } from "grpc-web-client";
import debug from "debug";
import { Promise } from "es6-promise";

export default class Client {
  host: string;

  constructor(host: string) {
    this.host = host;
  }

  request<T extends Message>(
    method: grpc.UnaryMethodDefinition<any, Message>,
    request: Message
  ): Promise<T> {
    const serviceName = method.service.serviceName.split(".");
    const serviceShortName = serviceName[serviceName.length - 1];

    const log = debug(\`api:\${serviceShortName}:\${method.methodName}\`);

    return new Promise<T>((resolve, reject) => {
      grpc.unary(method, {
        request: request,
        host: this.host,
        metadata: {
          authorization: \`Bearer \${localStorage.access_token}\`
        },
        onEnd: res => {
          const { status, statusMessage, headers, message, trailers } = res;
          log("status", status, statusMessage);
          log("headers", headers);
          if (status === grpc.Code.OK && message) {
            log("message", message.toObject());
            resolve(<T>message);
          } else {
            reject(statusMessage);
          }
          log("trailers", trailers);
        }
      });

      log("requested");
    });
  }
}
`
  });

  return files;
});
