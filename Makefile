test:
	mkdir -p tmp/_proto
	protoc \
		--plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts \
		--plugin=protoc-gen-vue=./cli.js \
		-I . \
		--js_out=import_style=commonjs,binary:./tmp/_proto \
		--ts_out=service=true:./tmp/_proto \
		--vue_out=./tmp \
		./test.proto \